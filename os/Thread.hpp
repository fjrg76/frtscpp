/*Copyright (C) 
 * 2018 - fjrg76 at gmail dot com
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#ifndef  THREAD_INC
#define  THREAD_INC

#include "FreeRTOS.h"
#include "task.h"

namespace os
{

class Thread
{
public:
	enum class Priority : uint8_t
	{ 
		low = tskIDLE_PRIORITY, /**< For tasks which execution is not critical */
		normal = 1, /**< For regular tasks */
		high = 2, /**< For tasks which execution is critical */
	};

	/**
	 * @brief Creates a task.
	 *
	 * @param _stackDepth stack depth in WORDS
	 * @param _priority Priority level
	 * @param _name Task name
	 */
	Thread( unsigned portSHORT _stackDepth, UBaseType_t _priority, const char* _name = "" )
	{
		xTaskCreate( task, _name, _stackDepth, this, _priority, &this->handle );

#if INCLUDE_vTaskDelayUntil == 1
		this->previousWakeTime = xTaskGetTickCount();
#endif  
	}

	/**
	 * @brief Creates a task.
	 *
	 * @param _stackDepth stack depth in WORDS
	 * @param _priority Priority level, \see Priority
	 * @param _name Task name
	 */
	Thread( unsigned portSHORT _stackDepth, Thread::Priority _priority = Thread::Priority::low, const char* _name = "" )
	{
		xTaskCreate( task, _name, _stackDepth, this, static_cast<UBaseType_t>( _priority ), &this->handle );

#if INCLUDE_vTaskDelayUntil == 1
		this->previousWakeTime = xTaskGetTickCount();
#endif  
	}

	/**
	 * @brief Main loop.
	 */
	virtual void Main() = 0;


	/**
	 * @brief Returns the task handle.
	 *
	 * @return The task handle
	 */
	TaskHandle_t GetHandle()
	{
		return this->handle;
	}

#if INCLUDE_vTaskDelete == 1
	/**
	 * @brief Deletes itself.
	 *
	 * @pre The task hasn't been deleted before
	 * @post The handle is set to nullptr
	 */
	void Delete()
	{
		configASSERT( this->handle );
		vTaskDelete( nullptr );
		this->handle = nullptr;
	}

	/**
	 * @brief Deletes other task or itself.
	 *
	 * @param _taskHandle The task handle of the task to be deleted. It's
	 * recommended to use \see Delete() without arguments to delete itself.
	 */
	void Delete( TaskHandle_t _taskHandle )
	{
		vTaskDelete( _taskHandle );
	}
#endif  


#if INCLUDE_vTaskPrioritySet == 1
	/**
	 * @brief Sets a new priority level for THIS task.
	 *
	 * @param _newPriority The new priority level
	 */
	void PrioritySet( UBaseType_t _newPriority )
	{
		vTaskPrioritySet( nullptr, _newPriority );
	}

	/**
	 * @brief Sets a new priority level for a task
	 *
	 * @param _taskHandle The task handle of the task
	 * @param _newPriority The new priority level
	 */
	void PrioritySet( TaskHandle_t _taskHandle, UBaseType_t _newPriority )
	{
		vTaskPrioritySet( _taskHandle, _newPriority );
	}
#endif  


#if INCLUDE_uxTaskPriorityGet == 1
	UBaseType_t PriorityGet()
	{
		return uxTaskPriorityGet( this->handle );
	}

	UBaseType_t PriorityGet( TaskHandle_t _task )
	{
		return uxTaskPriorityGet( _task );
	}
#endif  


#if INCLUDE_vTaskSuspend == 1
	void Suspend()
	{
		vTaskSuspend( nullptr );
	}

	void Suspend( TaskHandle_t _task )
	{
		vTaskSuspend( _task );
	}

	void Resume( TaskHandle_t _task )
	{
		vTaskResume( _task );
	}

	void ResumeFromISR( TaskHandle_t _task )
	{
		auto aTaskShouldBeWoken = xTaskResumeFromISR( _task );
		portYIELD_FROM_ISR( aTaskShouldBeWoken );
	}
#endif  


#if INCLUDE_vTaskDelay == 1
	void Sleep( TickType_t _timeToSleep_in_ms )
	{
		vTaskDelay( pdMS_TO_TICKS( _timeToSleep_in_ms ) );
	}
#endif  


#if INCLUDE_vTaskDelayUntil == 1
	/**
	 * @brief The task is blocked for the specified amount of time.
	 *
	 * @param _timeToSleep_in_ms Time to block in milliseconds.
	 */
	void SleepUntil( TickType_t _timeToSleep_in_ms )
	{
		vTaskDelayUntil( &this->previousWakeTime, pdMS_TO_TICKS( _timeToSleep_in_ms ) );
	}

	/**
	 * @brief Starts the SleepUntil mechanism.
	 *
	 * Must be used before \see SleepUntil() is called for the first time, or
	 * whenever a long time has ocurred since the last time such function was
	 * called.
	 */
	void SleepUntilStart()
	{
		this->previousWakeTime = xTaskGetTickCount();
	}

#endif  

private:
	static void task( void* _params )
	{
		Thread* p = static_cast<Thread*>( _params );
		p->Main();
	}

	TaskHandle_t handle = nullptr;


#if INCLUDE_vTaskDelayUntil == 1
	TickType_t previousWakeTime = 0;
#endif  
};

} // namespace os

#endif   /* ----- #ifndef THREAD_INC  ----- */
