/*Copyright (C) 
 * 2018 - fjrg76 at gmail dot com
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


#ifndef  THREADX_INC
#define  THREADX_INC

#include "FreeRTOS.h"
#include "task.h"

namespace os
{

template<typename T>
class ThreadX
{
public:
	ThreadX( unsigned portSHORT _stackDepth, UBaseType_t _priority, const char* _name = "" );

	TaskHandle_t GetHandle();

	void Main();

private:
	static void task( void* _params );

	TaskHandle_t taskHandle;
};


template<typename T>
ThreadX<T>::ThreadX( unsigned portSHORT _stackDepth, UBaseType_t _priority, const char* _name )
{
	xTaskCreate( task, _name, _stackDepth, this, _priority, &this->taskHandle );
}

template<typename T>
TaskHandle_t ThreadX<T>::GetHandle()
{
	return this->taskHandle;
}

template<typename T>
void ThreadX<T>::Main()
{
	static_cast<T&>( *this ).Main();
}

template<typename T>
void ThreadX<T>::task( void* _params )
{
	ThreadX* p = static_cast<ThreadX*>( _params );
	p->Main();
}

} // namespace os

#endif   /* ----- #ifndef THREADX_INC  ----- */
