/*Copyright (C) 
 * 2018 - fjrg76 at gmail dot com
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#ifndef  EVENTNOTIFY_INC
#define  EVENTNOTIFY_INC

#include "FreeRTOS.h"
#include "task.h"

/** @addtogroup Semaphores
 * @{
 */

namespace os
{

/**
 * @brief Implements a light-weight event-to-thread notification mechanism.
 */
class EventNotify
{
public:
	/**
	 * @brief Use this constructor when the thread's handle is already known.
	 *
	 * If later on you might want to change the receiver's thread, then you can
	 * use SetThreadToNotifyHandle() function.
	 *
	 * @param _thread2BNotified Handle of the thread that is going to be notified.
	 */
	explicit EventNotify ( TaskHandle_t _thread2BNotified ) :
		threadToNotify{ _thread2BNotified }
	{
		configASSERT( this->threadToNotify );
	}

	/**
	 * @brief Use this constructor when the object is instanciated from the
	 * global space. For example, you don't already know the consumer.
	 *
	 * Later on you must bind this object with the consumer that is going to be
	 * notified using SetThreadToNotifyHandle() function.
	 *
	 */
	EventNotify()
	{

	}

	/**
	 * @brief Binds the notifier to a thread.
	 *
	 * @param _thread2BNotified Handle of the thread that is going to be notified.
	 */
	void SetThreadToNotifyHandle( TaskHandle_t _thread2BNotified )
	{
		this->threadToNotify = _thread2BNotified;
	}

	/**
	 * @brief Waits until either an event happens or the time-out expires
	 *
	 * @param _timeOut Time-out in milliseconds. If 0, then it returns
	 * immediatly if the semaphore isn't available.
	 *
	 * @return true if an event happens BEFORE time-out had expired.\n
	 *		   false Time-out happens BEFORE an event had happened.
	 */
	bool Wait( TickType_t _timeOut )
	{
		configASSERT( this->threadToNotify );
		return ulTaskNotifyTake( pdTRUE, pdMS_TO_TICKS( _timeOut ) ) == 1;
	}

#if INCLUDE_vTaskSuspend == 1
	/**
	 * @brief Waits forever until an event happens
	 */
	void Wait()
	{
		configASSERT( this->threadToNotify );
		ulTaskNotifyTake( pdTRUE, portMAX_DELAY );
	}
#endif  

	/**
	 * @brief Notifies that an event has happened
	 *
	 * It wakes-up the Thread that was previously blocked waiting for the event.
	 *
	 * If the event happens inside an ISR then you must use NotifyFromISR()
	 * function instead 
	 *
	 * \see NotifyFromISR
	 */
	void Notify()
	{
		configASSERT( this->threadToNotify );
		xTaskNotifyGive( this->threadToNotify );
	}

	/**
	 * @brief Notifies that an event has happened from inside an ISR
	 *
	 * It wakes-up the Thread that was previously blocked waiting for the event.
	 */
	void NotifyFromISR()
	{
		BaseType_t need2bWoken{ pdFALSE };
		vTaskNotifyGiveFromISR( this->threadToNotify, &need2bWoken );
		portYIELD_FROM_ISR( need2bWoken );
	}
private:
	TaskHandle_t threadToNotify{ nullptr };
};

} // namespace( os )

/**@}*/

#endif   /* ----- #ifndef EVENTNOTIFY_INC  ----- */
