/*Copyright (C) 
 * 2018 - fjrg76 at gmail dot com
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


#ifndef  BINARY_INC
#define  BINARY_INC

#if configUSE_COUNTING_SEMAPHORES == 1

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "ISemaphore.hpp"


/** @addtogroup Semaphores
 * @{
 */

namespace os
{

/**
 * @brief Implements a binary semaphore
 */
class Binary : public ISemaphore
{
public:
	/**
	 * @brief Creates a binary semaphore
	 */
	Binary()
	{
		this->handle = xSemaphoreCreateBinary();
	}
};

} // namespace( os )

/**@}*/

#endif  

#endif   /* ----- #ifndef BINARY_INC  ----- */
