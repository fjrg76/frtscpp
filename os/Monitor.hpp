/*Copyright (C) 
 * 2018 - fjrg76 at gmail dot com
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


#ifndef  MONITOR_INC
#define  MONITOR_INC

#if 0 
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"
#endif  

#include "Mutex.hpp"


#if configUSE_MUTEXES == 1

/** @addtogroup Semaphores
 * @{
 */

namespace os {

/**
 * @brief Implements a monitor. It isn't supposed to be used directly
 */
class Monitor
{
protected:
	Mutex mutex;
	TickType_t timeOut;

	/**
	 * @brief Creates a monitor
	 *
	 * @param _timeOut Sets a default timeout for all threads that want to
	 * access the underlying resource
	 */
	Monitor( TickType_t _timeOut ) :
		timeOut{ _timeOut }
	{
		// nothing
	}

public:
	/**
	 * @brief Sets a new timeout
	 *
	 * @param _timeOut New timeout
	 */
	void SetTimeOut( TickType_t _timeOut )
	{
		this->timeOut = _timeOut;
	}

	/**
	 * @brief Return whether the resource is locked
	 *
	 * @return true if the resource is locked
	 */
	bool isLocked() const
	{
		return mutex.IsLocked();
	}

	bool wasCreated() const
	{
		return mutex.GetHandle() != nullptr;
	}
};

} // namespace( os )

/**@}*/
#else
#error "configUSE_MUTEXES should be set in order to use this class!"
#endif

#endif   /* ----- #ifndef MONITOR_INC  ----- */
