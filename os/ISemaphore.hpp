/*Copyright (C) 
 * 2018 - fjrg76 at gmail dot com
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


#ifndef  ISEMAPHORE_INC
#define  ISEMAPHORE_INC

#if configUSE_COUNTING_SEMAPHORES == 1

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

namespace os
{

/**
 * @defgroup Semaphores Family of related semaphores
 */
/**@{*/

class ISemaphore
{
public:

	/**
	 * @brief Deletes the semaphore
	 */
	void Delete()
	{
		configASSERT( this->handle );
		vSemaphoreDelete( this->handle );
		this->handle = nullptr;
	}

	/**
	 * @brief Gives the semaphore.
	 *
	 * @return true if the semaphore was released.\nfalse otherwise. 
	 *
	 * @note (sic)"An error can occur if there is no space on the queue to post a message - indicating that the semaphore was not first obtained correctly." (see https://www.freertos.org/a00123.html)
	 */
	bool Give()
	{
		configASSERT( this->handle );
		return xSemaphoreGive( this->handle ) == pdTRUE;
	}

	/**
	 * @brief Releases a semaphore from an ISR.
	 *
	 * @pre The semaphore exists.
	 * @pre The semaphore was taken.
	 * @post A context switch is performed if a thread with higher priority is
	 * ready to run.
	 */
	void GiveFromISR()
	{
		configASSERT( this->handle );
		BaseType_t need2bWoken{ pdFALSE };
		xSemaphoreGiveFromISR( this->handle, &need2bWoken );
		portYIELD_FROM_ISR( need2bWoken );
	}

	/**
	 * @brief Takes the semaphore.
	 *
	 * @param _timeOut Time in milliseconds that the thread needs to wait to be
	 * unblocked.
	 *
	 * @return true if the sempahore was given before time-out expires.
	 *
	 * @note DON'T USE THIS FUNCTION FROM INSIDE AN ISR. Use the function TakeFromISR() instead. 
	 *
	 * @pre The semaphore exists.
	 * @post The thread that took the semaphore is blocked until: the semaphore
	 * is given or the time-out expires.
	 */
	bool Take( TickType_t _timeOut )
	{
		configASSERT( this->handle );
		return xSemaphoreTake( this->handle, _timeOut ) == pdTRUE;
	}

	/**
	 * @brief Takes the semaphore from an ISR.
	 *
	 * @return true if the sempahore was taken.
	 *
	 * @pre The semaphore exists.
	 * @post The thread that took the semaphore is blocked until the semaphore is given.
	 * @post A context switch is performed if a thread with higher priority is
	 * ready to run.
	 */
	bool TakeFromISR()
	{
		configASSERT( this->handle );
		BaseType_t need2bWoken{ pdFALSE };
		auto res = xSemaphoreTakeFromISR( this->handle,	&need2bWoken );
		portYIELD_FROM_ISR( need2bWoken );
		return res == pdTRUE;
	}

	/**
	 * @brief Returns the count of the semaphore
	 *
	 * @return The semaphore's count. If it's a counting one, then the current
	 * value is returned. If the semaphore is a binary one, then 1 is returned
	 * if the semaphore is available, and 0 if it's not.
	 */
	UBaseType_t GetCount()
	{
		configASSERT( this->handle );
		return uxSemaphoreGetCount( this->handle );
	}


protected:
	SemaphoreHandle_t handle{ nullptr };

	ISemaphore(){}
	// we don't want instances from this class!
};

} // namespace( os )

/**@}*/

#endif  

#endif   /* ----- #ifndef ISEMAPHORE_INC  ----- */
