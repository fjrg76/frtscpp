/*Copyright (C) 
 * 2018 - fjrg76 at gmail dot com
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */
 
#ifndef  MUTEX_INC
#define  MUTEX_INC

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"



#if configUSE_MUTEXES == 1

/** @addtogroup Semaphores
 * @{
 */

namespace os {

/**
 * @brief Implements a mutex
 */
class Mutex
{
public:
	/**
	 * @brief Creates a mutex
	 */
	Mutex()
	{
		this->handle = xSemaphoreCreateMutex();
	}

	/**
	 * @brief Locks the mutex
	 *
	 * @param _owner The handle of the thread that is going to own the mutex
	 * @param _timeOut Time in milliseconds the thread needs to wait to get the
	 * mutex
	 *
	 * @return true if the mutex was obtained. \nfalse if timeout expired before
	 * the mutex was available
	 *
	 * @pre The mutex exists
	 * @pre The thread that wants to lock the mutex exists
	 */
	bool Lock( const TaskHandle_t _owner, TickType_t _timeOut )
	{
		configASSERT( _owner );
		configASSERT( this->handle );

		if( xSemaphoreTake( this->handle, _timeOut ) == pdFALSE ){ return false; }
		this->owner = _owner;
		return true;
	}

	/**
	 * @brief Locks the mutex from an ISR
	 *
	 * @param _owner The handle of the thread that is going to own the mutex
	 *
	 * @return true if the mutex was obtained
	 */
	bool LockFromISR( const TaskHandle_t _owner )
	{
		configASSERT( _owner );
		configASSERT( this->handle );

		BaseType_t need2bWoken = pdFALSE;
		if( xSemaphoreTakeFromISR( this->handle, &need2bWoken ) == pdFALSE ){ return false; }
		this->owner = _owner;
		portYIELD_FROM_ISR( need2bWoken );
		return true;
	}

	// bool Lock( const TaskHandle_t _owner, 
	//		return immediatly OR
	//		wait forever ){}

	/**
	 * @brief Unlocks the mutex
	 *
	 * @param _owner The thread that locked the mutex
	 *
	 * @return true if the mutex was succesfully released
	 *
	 * @note The mutex might not be released if: a thread other than the one that
	 * locked the mutex tries to release it; or the mutex wasn't obtained
	 * correctly in the first place (see https://www.freertos.org/a00123.html )
	 */
	bool Unlock( const TaskHandle_t _owner )
	{
		configASSERT( _owner );
		configASSERT( this->handle );

		// alternativa usando la api oficial:
		// if( _owner == xSemaphoreGetMutexHolder(this->handle)){

		if( _owner == this->owner and xSemaphoreGive( this->handle ) == pdTRUE ){
			this->owner = nullptr;
			return true;
		}

		return false;
	}

	/**
	 * @brief Returns the handle of the task that owns the mutex
	 *
	 * @return The handle of the task that currently owns the mutex, if any
	 */
	TaskHandle_t GetOwner() const
	{
		return this->owner;
	}

	/**
	 * @brief Return whether the mutex is locked
	 *
	 * @return true if the mutex is locked
	 *
	 * @note This function doesn't block
	 */
	bool IsLocked() const
	{
		return this->owner != nullptr;
	}

	/**
	 * @brief Returns the handle of the mutex
	 *
	 * @return The handle of the mutex
	 *
	 * @note What is returned is a const reference of the handle, so that the 
	 * client cannot modify it. This function is intended for comparing purposes only
	 */
	const SemaphoreHandle_t GetHandle() const
	{
		return this->handle;
	}

private:
	TaskHandle_t owner{ nullptr };       //!< Mutex owner
	SemaphoreHandle_t handle{ nullptr }; //!< Mutex handler
};


} // namespace( os )

/**@}*/

#endif // configUSE_MUTEXES

#endif   /* ----- #ifndef MUTEX_INC  ----- */
