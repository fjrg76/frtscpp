/*Copyright (C) 
 * 2018 - fjrg76 at gmail dot com
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


#ifndef  SEMAPHORE_INC
#define  SEMAPHORE_INC

#if configUSE_COUNTING_SEMAPHORES == 1

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#include "ISemaphore.hpp"

/**
 * @addtogroup Semaphores 
 */
/**@{*/

namespace os
{

/**
 * @brief Implements a (counting) semaphore
 */
class Semaphore : public ISemaphore
{
public:
	/**
	 * @brief Creates a semaphore
	 *
	 * @param _maxCount The maximum count value that can be reached
	 * @param _initialCount The count value assigned to the sempahores when it's
	 * created. Default initial value is 0
	 */
	explicit Semaphore ( UBaseType_t _maxCount, UBaseType_t _initialCount = 0 ) :
		maxCount{ _maxCount }
	{
		this->handle = xSemaphoreCreateCounting( _maxCount, _initialCount );
	}

	/**
	 * @brief Returns the maximum count value that can be reached
	 *
	 * @return The maximum count value that can be reached
	 */
	UBaseType_t GetMaxCount() const
	{
		return this->maxCount;
	}

private:
	UBaseType_t maxCount{ 0 };
};

} // namespace( os )

/**@}*/

#endif  

#endif   /* ----- #ifndef SEMAPHORE_INC  ----- */
