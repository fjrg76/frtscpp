
/*Copyright (C) 
 * 2018 - fjrg76 at gmail dot com
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */


#ifndef  TIMER_INC
#define  TIMER_INC

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

namespace os
{

class Timer
{
public:
	/**
	 * @brief Timer type.
	 */
	enum class Type : uint8_t 
	{ 
		one_shot = 0, /**< One shot timer */
		reload, /**< Run forever timer */
	};

	/**
	 * @brief The user function that is going to be called everytime the timer
	 * expires.
	 */
	virtual void Callback() = 0;

	/**
	 * @brief Starts the timer.
	 *
	 * @param _blockTime Time delay (in ticks) between the moment this function
	 * is execute and when the command actually is received. For more info,
	 * please see the FreeRTOS official documentation.
	 * https://www.freertos.org/FreeRTOS-timers-xTimerStart.html
	 *
	 * @return true if the command was succesfully sent to the timer\n
	 *		   false if the start command could not be sent to the timer even
	 *		   after the block time had passed.
	 */
	bool Start( TickType_t _blockTime = 0 )
	{
		return xTimerStart( this->handle, _blockTime ) == pdPASS;
	}

	/**
	 * @brief 
	 *
	 * @param _blockTime
	 *
	 * @return 
	 */
	bool Stop( TickType_t _blockTime = 0 )
	{
		return xTimerStop( this->handle, _blockTime ) == pdPASS;
	}

	/**
	 * @brief 
	 *
	 * @param _blockTime
	 *
	 * @return 
	 */
	bool Reset( TickType_t _blockTime = 0 )
	{
		return xTimerReset( this->handle, _blockTime ) == pdPASS;
	}

	/**
	 * @brief 
	 *
	 * @param _newPeriod
	 * @param _blockTime
	 *
	 * @return 
	 */
	bool ChangePeriod( TickType_t _newPeriod, TickType_t _blockTime = 0 )
	{
		return xTimerChangePeriod( this->handle, _newPeriod, _blockTime ) == pdPASS;
	}

	/**
	 * @brief 
	 *
	 * @return 
	 */
	bool IsActive()
	{
		return xTimerIsTimerActive( this->handle ) == pdTRUE;
	}

	/**
	 * @brief 
	 *
	 * @return 
	 */
	TickType_t GetExpiryTime()
	{
		return xTimerGetExpiryTime( this->handle );
	}

	size_t GetID() const
	{
		return this->id;
	}


protected:
	Timer( TickType_t _period, Timer::Type _type = Timer::Type::reload, const char* _name = "" )
	{
		this->handle = xTimerCreate(
				_name,
				pdMS_TO_TICKS( _period ),
				static_cast<UBaseType_t>( _type ),
				this,
				callback );
	}

	static void callback( TimerHandle_t _handle )
	{
		Timer* p = static_cast<Timer*>( pvTimerGetTimerID( _handle ) );
		p->Callback();
	}

	TimerHandle_t handle;
	size_t id;
};

} // namespace os

#endif   /* ----- #IFNDEF TIMER_INC  ----- */
