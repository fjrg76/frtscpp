/*Copyright (C) 
 * 2018 - fjrg76 at gmail dot com
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

//
// This demo toggles a LED using threads, notifiers and the system tick
//


#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include "FreeRTOS.h"
#include "task.h"
#include "timers.h"

#include "Thread.hpp"
#include "TaskNotify.hpp"

uint32_t counter = 0;
TaskNotify aNotifier;


class Client : public Thread
{
public:
	Client( uint8_t _port, uint8_t _pin ) :
		Thread{ 128, tskIDLE_PRIORITY, "RCVR" },
		port{ _port }, pin{ _pin }
	{
		Chip_GPIO_SetPinDIROutput( LPC_GPIO, this->port, this->pin );
	}

	virtual void Main() override
	{
		while( 1 )
		{
			Chip_GPIO_SetPinOutHigh( LPC_GPIO, this->port, this->pin );
			counter = 500;
			aNotifier.Wait();
			vTaskDelay( 500 );
		}
	}

private:
	uint8_t port;
	uint8_t pin;
};

//----------------------------------------------------------------------
int main(void) 
{

	SystemCoreClockUpdate();
	Board_Init();

	Client client( 0, 7 );
	aNotifier.SetTaskToNotifyHandle( client.GetHandle() );

	vTaskStartScheduler();

	return 0 ;
}


//-------------------- C plain stuff -------------------------------------------
#ifdef __cplusplus
extern "C"{
#endif

void vApplicationMallocFailedHook( void )
{
	taskDISABLE_INTERRUPTS();
	for( ;; );
}

void vApplicationIdleHook( void )
{
}

void vApplicationTickHook( void )
{
	if( counter > 0 ){
		--counter;
		if( counter == 0 ){
			Chip_GPIO_SetPinOutLow( LPC_GPIO, 0, 7 );
			aNotifier.NotifyFromISR();
		}
	}
}

void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed portCHAR *pcTaskName )
{
	configASSERT( 0 );
}

/* configSUPPORT_STATIC_ALLOCATION is set to 1, so the application must provide an
   implementation of vApplicationGetIdleTaskMemory() to provide the memory that is
   used by the Idle task. */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer,
		StackType_t **ppxIdleTaskStackBuffer,
		uint32_t *pulIdleTaskStackSize )
{
	/* If the buffers to be provided to the Idle task are declared inside this
	   function then they must be declared static - otherwise they will be allocated on
	   the stack and so not exists after this function exits. */
	static StaticTask_t xIdleTaskTCB;
	static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

	/* Pass out a pointer to the StaticTask_t structure in which the Idle task's
	   state will be stored. */
	*ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

	/* Pass out the array that will be used as the Idle task's stack. */
	*ppxIdleTaskStackBuffer = uxIdleTaskStack;

	/* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
	   Note that, as the array is necessarily of type StackType_t,
	   configMINIMAL_STACK_SIZE is specified in words, not bytes. */
	*pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
/*-----------------------------------------------------------*/

/* configSUPPORT_STATIC_ALLOCATION and configUSE_TIMERS are both set to 1, so the
   application must provide an implementation of vApplicationGetTimerTaskMemory()
   to provide the memory that is used by the Timer service task. */
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer,
		StackType_t **ppxTimerTaskStackBuffer,
		uint32_t *pulTimerTaskStackSize )
{
	/* If the buffers to be provided to the Timer task are declared inside this
	   function then they must be declared static - otherwise they will be allocated on
	   the stack and so not exists after this function exits. */
	static StaticTask_t xTimerTaskTCB;
	static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

	/* Pass out a pointer to the StaticTask_t structure in which the Timer
	   task's state will be stored. */
	*ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

	/* Pass out the array that will be used as the Timer task's stack. */
	*ppxTimerTaskStackBuffer = uxTimerTaskStack;

	/* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
	   Note that, as the array is necessarily of type StackType_t,
	   configTIMER_TASK_STACK_DEPTH is specified in words, not bytes. */
	*pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}


#ifdef __cplusplus
}
#endif
//------------------------------------------------------------------------------
