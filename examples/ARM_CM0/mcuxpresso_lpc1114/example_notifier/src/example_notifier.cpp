/*Copyright (C) 
 * 2018 - fjrg76 at gmail dot com
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * 
 */

#if defined (__USE_LPCOPEN)
#if defined(NO_BOARD_LIB)
#include "chip.h"
#else
#include "board.h"
#endif
#endif

#include <cr_section_macros.h>

#include "Thread.hpp"
#include "EventNotify.hpp"

using namespace os;

uint16_t gAdcVal{ 0 };
// adc data read

os::EventNotify eNotify;
// the event notifier



void AnalogInit()
{
	ADC_CLOCK_SETUP_T ADCSetup;
	Chip_IOCON_PinMuxSet(LPC_IOCON, IOCON_PIO0_11, FUNC2);
	Chip_ADC_Init(LPC_ADC, &ADCSetup);
}




//----------------------------------------------------------------------
//  Class Led
//----------------------------------------------------------------------
struct Led final : Thread
{
	Led(	uint8_t _port, 
			uint8_t _pin, 
			unsigned portSHORT _stackDepth = configMINIMAL_STACK_SIZE,
			Thread::Priority _priority = os::Thread::Priority::low, 
			const char* _name = "LED" ) :
		Thread{ _stackDepth, _priority, _name },
		port{ _port }, pin{ _pin }
	{
		Chip_GPIO_SetPinDIROutput( LPC_GPIO, this->port, this->pin );
		eNotify.SetThreadToNotifyHandle( GetHandle() );
	}

	virtual void Main() override;

private:
	uint8_t port{ 0 };
	uint8_t pin{ 0 };
};

void Led::Main() 
{
	while( 1 )
	{
		eNotify.Wait();
		Chip_GPIO_SetPinState( LPC_GPIO, this->port, this->pin, gAdcVal > 512 ? 1 : 0 );
	}
}

//----------------------------------------------------------------------
//  Clas Adc
//----------------------------------------------------------------------
struct Adc : Thread
{
	Adc( ADC_CHANNEL_T _channel, 
			unsigned portSHORT _stackDepth = configMINIMAL_STACK_SIZE,
			Thread::Priority _priority = os::Thread::Priority::low, 
			const char* _name = "ADC" ) :
		Thread{ _stackDepth, _priority, _name },
		channel{ _channel }
	{
		Chip_ADC_EnableChannel( LPC_ADC, this->channel, ENABLE );
	}

	virtual void Main() override;

private:
	ADC_CHANNEL_T channel;
};

void Adc::Main()
{
	while( 1 )
	{
		Sleep( 5 );
		Chip_ADC_SetStartMode( LPC_ADC, ADC_START_NOW, ADC_TRIGGERMODE_RISING );
		while( Chip_ADC_ReadStatus(LPC_ADC, this->channel, ADC_DR_DONE_STAT) != SET ){ ; }
		uint16_t dataADC;
		Chip_ADC_ReadValue(LPC_ADC, this->channel, &gAdcVal );
		eNotify.Notify();
	}
}



int main(void) {

	SystemCoreClockUpdate();
	Board_Init();
	Board_LED_Set(0, true);
	AnalogInit();

	Led led1{ 0, 7 };
	Adc adc{ ADC_CH0, 128, Thread::Priority::normal };

	vTaskStartScheduler();

}


//-------------------- C plain stuff -------------------------------------------
#ifdef __cplusplus
extern "C"{
#endif

void vApplicationMallocFailedHook( void )
{
	taskDISABLE_INTERRUPTS();
	for( ;; );
}

void vApplicationIdleHook( void )
{
}

void vApplicationTickHook( void )
{
#if 0
	if( counter > 0 ){
		--counter;
		if( counter == 0 ){
			Chip_GPIO_SetPinOutLow( LPC_GPIO, 0, 7 );
			aSemaphore.NotifyFromISR();
		}
	}
#endif
}

void vApplicationStackOverflowHook( xTaskHandle *pxTask, signed portCHAR *pcTaskName )
{
	configASSERT( 0 );
}

/* configSUPPORT_STATIC_ALLOCATION is set to 1, so the application must provide an
   implementation of vApplicationGetIdleTaskMemory() to provide the memory that is
   used by the Idle task. */
void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer,
		StackType_t **ppxIdleTaskStackBuffer,
		uint32_t *pulIdleTaskStackSize )
{
	/* If the buffers to be provided to the Idle task are declared inside this
	   function then they must be declared static - otherwise they will be allocated on
	   the stack and so not exists after this function exits. */
	static StaticTask_t xIdleTaskTCB;
	static StackType_t uxIdleTaskStack[ configMINIMAL_STACK_SIZE ];

	/* Pass out a pointer to the StaticTask_t structure in which the Idle task's
	   state will be stored. */
	*ppxIdleTaskTCBBuffer = &xIdleTaskTCB;

	/* Pass out the array that will be used as the Idle task's stack. */
	*ppxIdleTaskStackBuffer = uxIdleTaskStack;

	/* Pass out the size of the array pointed to by *ppxIdleTaskStackBuffer.
	   Note that, as the array is necessarily of type StackType_t,
	   configMINIMAL_STACK_SIZE is specified in words, not bytes. */
	*pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
}
/*-----------------------------------------------------------*/

/* configSUPPORT_STATIC_ALLOCATION and configUSE_TIMERS are both set to 1, so the
   application must provide an implementation of vApplicationGetTimerTaskMemory()
   to provide the memory that is used by the Timer service task. */
void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer,
		StackType_t **ppxTimerTaskStackBuffer,
		uint32_t *pulTimerTaskStackSize )
{
	/* If the buffers to be provided to the Timer task are declared inside this
	   function then they must be declared static - otherwise they will be allocated on
	   the stack and so not exists after this function exits. */
	static StaticTask_t xTimerTaskTCB;
	static StackType_t uxTimerTaskStack[ configTIMER_TASK_STACK_DEPTH ];

	/* Pass out a pointer to the StaticTask_t structure in which the Timer
	   task's state will be stored. */
	*ppxTimerTaskTCBBuffer = &xTimerTaskTCB;

	/* Pass out the array that will be used as the Timer task's stack. */
	*ppxTimerTaskStackBuffer = uxTimerTaskStack;

	/* Pass out the size of the array pointed to by *ppxTimerTaskStackBuffer.
	   Note that, as the array is necessarily of type StackType_t,
	   configTIMER_TASK_STACK_DEPTH is specified in words, not bytes. */
	*pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
}


#ifdef __cplusplus
}
#endif
//------------------------------------------------------------------------------
