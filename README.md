**frtscpp (pronounced: freertos C++)**

## Introduction

The aim of this project is to objectify the well know kernel [FreeRTOS](https://www.freertos.org). 

There are a number of reasons for doing so: 

1. C++ 11+ rocks! 
2. C++ is safer than C.
3. FreeRTOS is crying out for being objectified! 

Points 1 and 2 are self-explanatory; point 3 isn't. For a non trivial applications based on FreeRTOS you end up with dozens of state variables, most of them in the global space distributed everywhere. Besides that, all these state variables are used exclusively for the kernel functions. So, data + operations on that data = ADT (abstract data type). In OOL and OOP an ADT is a class. Is this explanation enough for objectifying the kernel? I hope so!

There's a lema a like to follow: *"If there are state variables that must be
kept along the program's life, then they are excellent candidates for classes."*

I haven't touched the kernel internals, that's insane! Instead, I've created a
C++ wrapper classes so all the kernel functionality is available through C++
classes. *Note: There is a lot of work to be done; however, anyone can start
writing applications using classes.*

**The wrapper classes are independent from any particular architecture.**
Examples (so far) are not.

## Tools

C++ 11 (from GCC) has being used through all this work. Older versions won't compile.
MCUXpresso (from NXP) and the LPC1114 (Cortex-M0) chip has been used so far.
This project should work with similar IDEs and chips.

I'm not an expert in C++ 11+, but if you are and you're willing to, you might
let me know how I can improved this project.

Also, you need to know how to link external sources to your own projects.

## Examples

Some examples are included, all of them (by the time being) are focused on the
MCUXpresso IDE and the LPC1114 chip. The idea is to include as many
architectures as possible.

## Who is this project for

I'm crazy about objects, everything I see are objects. If you don't like to
program with objects, or if you are a C purist, then this project might not be for you.

As stated earlier, there is a lot of work to be done, so you

## Documentation

I've used Doxygen for the documentation.

## What's next

1. To objectify ALL FreeRTOS functionality.
2. Templetized some functionality so size and performance can be improved.
3. More examples.

**I would like to thank to [BitBucket](https://bitbucket.org) and [SourceForge](https://sourceforge.net) for let me share throug their tools this project with the community.**

---

## Clone the repository

Using git on a terminal, type (or copy-and-paste):

git clone https://fjrg76@bitbucket.org/fjrg76/frtscpp.git


or just download it from:

https://bitbucket.org/fjrg76/frtscpp/downloads/

clicking on **Download repository**


Another (not tested yet) way:

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You'll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you'd like to and then click **Clone**.
4. Open the directory you just created to see your repository's files.
